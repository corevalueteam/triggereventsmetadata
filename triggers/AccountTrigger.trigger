trigger AccountTrigger on Account (before insert, before update) {
	AccountTriggerEventsManager.onTriggerEvents(Account.sObjectType);
}