/**
 *	Virtual class containing methods to override that define trigger context. 
 *	@author Maksym Gorinshteyn
 */
public virtual class TriggerEvents {

	// context-specific methods for override
	public virtual void beforeInsert(){
	}

	public virtual void beforeUpdate(){
	}

	public virtual void beforeDelete(){
	}

	public virtual void afterInsert(){
	}

	public virtual void afterUpdate(){
	}

	public virtual void afterDelete(){
	}

	public virtual void afterUndelete(){
	}
}