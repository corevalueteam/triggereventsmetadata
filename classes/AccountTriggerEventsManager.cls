/**
 *	Used to instantiate and execute sObject's TriggerEvents specified by trigger settings.
 *	@author Maksym Gorinshteyn
 */
public class AccountTriggerEventsManager {
	/**	
	 *	Instantiates and executes sObject's TriggerEvents specified by trigger settings.
 	 *	@param 	sObjectType specifies trigger setting.
 	 */
	public static void onTriggerEvents(Schema.SObjectType sObjectType){
		TriggerEvents triggerEventsInstance = new TriggerEvents();
		String soType = String.valueOf(sObjectType).remove('__c');
		List<TriggerSettings__mdt> triggerSettings = [SELECT className__c,
															 order__c	 
			 								   		  FROM TriggerSettings__mdt 
											          WHERE sObjectType__c=:soType
											          ORDER BY order__c
											   		  ];
		for (TriggerSettings__mdt triggerSetting: triggerSettings){
			Type classType= Type.forName(triggerSetting.className__c);
			triggerEventsInstance = (TriggerEvents)classType.newInstance();// TODO
			execute(triggerEventsInstance);
		}
	}

	/**
 	 *	Dispatch to the correct triggerEventsInstance method.
 	 *	@param 	triggerEventsInstance Instance of TriggerEvents.
 	 */
	private static void execute(TriggerEvents triggerEventsInstance){
		if(Trigger.isBefore) {
			if (Trigger.isInsert){
				triggerEventsInstance.beforeInsert();
			}else if(Trigger.isUpdate){
				triggerEventsInstance.beforeUpdate();
			}else if(Trigger.isDelete){
				triggerEventsInstance.beforeDelete();
			}
		} else{
			if (Trigger.isInsert){
				triggerEventsInstance.afterInsert();
			}else if(Trigger.isUpdate){
				triggerEventsInstance.afterUpdate();
			}else if(Trigger.isDelete){
				triggerEventsInstance.afterDelete();
			}
		}
	}
}