/**
 *  Checks Account duplicates before upsert.
 *  @author Maksym Gorinshteyn
*/
public with sharing class AccountCheckDuplicates extends TriggerEvents {
    private Set<String> phones = new Set<String>();
    private Set<String> ratings = new Set<String>();
    private Set<Decimal> indexes = new Set<Decimal>();
    private Boolean checkNulls;

    public override void beforeInsert(){
        checkDuplicates(Trigger.new);
    }

    public override void beforeUpdate(){
        checkDuplicates(Trigger.new);
    }

    /**
    *   Invokes when before upsert trigger fires.
    *   @param newAccounts List of Account records for upsert.
    */
    public void checkDuplicates(List<Account> newAccounts){
        Map<AccountWrapper, Account> wrapperToNewAccount = new Map<AccountWrapper, Account>();
        
        // exists duplicate Account records from org
        List<Account> existAccounts = new List<Account>();
        wrapperToNewAccount = wrapAccounts(newAccounts);
        String query = 'SELECT Id, Phone, Rating, Index__c FROM Account WHERE ';
        if(System.Trigger.isUpdate){
            Set<ID> iDs = new Set<Id>();
            for(Account newAccount: newAccounts){
                iDs.add(newAccount.Id);
            }
            query = String.format(query+'{1}{0})',
                                    new String[]{
                                        '((Phone IN :phones AND Rating IN :ratings) OR (Index__c IN: indexes))',
                                        '((Id NOT IN: iDs) AND '
                                        }
                                    );
            existAccounts = Database.query(query);
        }
        else if(System.Trigger.isInsert){
            query = String.format(query+'{0}',
                                    new String[]{
                                        '((Phone IN :phones AND Rating IN :ratings) OR (Index__c IN: indexes))'
                                        }
                                    );
            existAccounts = Database.query(query);
        }
        
        findDuplicates(existAccounts, wrapperToNewAccount);
    }

    /**
    *   Generates the wrappers of Accounts for upsert.
    *   @param accounts List of Accounts for upsert.
    *   @return Map  AccountWrapper => Account.
    */
    private Map<AccountWrapper, Account> wrapAccounts(List<Account> accounts){
        Map<AccountWrapper, Account> wrapperToNewAccount = new Map<AccountWrapper, Account>();
        AccountWrapper accWrapper = new AccountWrapper();
        for(Account account : accounts){
            accWrapper.phone = account.Phone;
            accWrapper.rating = account.Rating;
            accWrapper.index = account.Index__c;
            phones.add(accWrapper.phone);
            ratings.add(accWrapper.rating);
            indexes.add(accWrapper.index);
            if(wrapperToNewAccount.containsKey(accWrapper)){
                account.addError(Label.Duplicate_record);
            }else{
                wrapperToNewAccount.put(accWrapper, account);
            }
        }
        return wrapperToNewAccount;

    }

    /**
    *   Checks for duplicate records of Accounts before upsert.
    *   @param accounts List of Account records from org.
    *   @param wrapperToNewAccount Map AccountWrapper => Account for upsert.
    */
    private void findDuplicates(List<Account> accounts, Map<AccountWrapper, Account> wrapperToNewAccount){
        AccountWrapper accWrapper = new AccountWrapper();
        for(Account account : accounts){
            accWrapper.phone = account.Phone;
            accWrapper.rating = account.Rating;
            accWrapper.index = account.Index__c;
            if(wrapperToNewAccount.containsKey(accWrapper)){
                Account newAccount = wrapperToNewAccount.get(accWrapper);
                if(((newAccount.Phone!=null)||(newAccount.Rating!=null))&&(newAccount.Index__c!=null)){
                    if((newAccount.Phone==accWrapper.phone) && (newAccount.Rating==accWrapper.rating)){
                        newAccount.Phone.adderror(String.format(Label.Account_exists,
                                                                new String[]{
                                                                    'Phone and Rating'
                                                                    }
                                                                ));
                        newAccount.Rating.adderror(String.format(Label.Account_exists,
                                                                new String[]{
                                                                    'Phone and Rating'
                                                                    }
                                                                ));
                    }
                    else{
                        newAccount.Index__c.adderror(String.format(Label.Account_exists,
                                                                new String[]{
                                                                    'Index'
                                                                    }
                                                                ));
                    }
                }
            }
        }
    }

    /**
    *  Account Wrapper
    */
    public class AccountWrapper{
        public String phone;
        public String rating;
        public Decimal index;

        public AccountWrapper(){

        }
        public AccountWrapper(String phone, String rating, Decimal index){
            this.phone = phone;
            this.rating = rating;
            this.index = index;
        }

        public Integer hashCode() {
            Integer phoneHashCode = phone == null ? 0 : phone.hashCode();
            Integer ratingHashCode = rating == null ? 0 : rating.hashCode();
            Integer indexHashCode = index== null ? 0 : (Integer)index;
            return (31 * phoneHashCode) ^ (indexHashCode * ratingHashCode);
            
        }

        public Boolean equals(Object obj){
            if(obj!= null && obj instanceOf AccountWrapper){
                AccountWrapper wrapper = (AccountWrapper) obj;
                if(((wrapper.phone == phone)&&(wrapper.rating == rating))||(wrapper.index == index)){
                    return true;
                }
            } 
            return false;
        }
    }
}